#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN


tourDescription = """
#Scapy Profile
"""
tourInstructions = """
N/A
"""
#Experimental Images
#Controller image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-Controller
#nextEPC eNb image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-eNb-epc
#no next eNB image: urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1


class GLOBALS(object):
    NUC_HWTYPE = "nuc5300"
    COTS_UE_HWTYPE = "nexus5"
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    CONTROL_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD" #Used Ubuntu20 for better python support
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1" #Low-latency image
    ENB_IMG = "urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1"
    COTS_UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")


pc = portal.Context()
#Allow the user to specify a specific UE
pc.defineParameter("FIXED_UE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a POWDER controlled RF UE node to allocate (e.g., 'ue1').  Leave blank to let the mapping algorithm choose.")
#Allow the user to specify a specific eNb
pc.defineParameter("FIXED_ENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a POWDER controlled RF eNodeB device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")


params = pc.bindParameters()

#Check for exceptions and warnings
pc.verifyParameters()

#Create model of the RSpec
request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb1 = request.RawPC("enb1")
if params.FIXED_ENB:
        enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
# enb1.disk_image = GLOBALS.SRSLTE_IMG
enb1.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"
enb1.Desire("rf-controlled", 1)
enb1_rue1_rf = enb1.addInterface("rue1_rf")
#enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
#enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
#enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
# enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/install_eNB.sh"))

# Add a UE node
rue1 = request.RawPC("rue1")
if params.FIXED_UE: #Use a specific UE
    rue1.component_id = params.FIXED_UE
rue1.hardware_type = GLOBALS.NUC_HWTYPE
rue1.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"
#rue1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
#rue1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))

rue1.Desire("rf-controlled", 1)
rue1_enb1_rf = rue1.addInterface("enb1_rf")

# Create the RF link between the UE and eNodeB
rflink = request.RFLink("rflink")
rflink.addInterface(enb1_rue1_rf)
rflink.addInterface(rue1_enb1_rf)

# Create the LAN and attach the devices
epclink = request.Link("s1-lan")
epclink.addNode(enb1)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
