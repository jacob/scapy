#!/bin/bash

echo "Setting Up Controller"

sudo apt-get update
sudo apt-get install -y python3-pip
yes y | pip3 install empower-core tornado construct pymodm influxdb python-stdnum

echo "                                                             Finished Pip Install"

sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install -y docker-ce

echo "                                                          Finished Docker Install"

sudo apt-get remove -y  --purge man-db
sudo apt-get update
sudo apt-get -y install mongodb
sudo systemctl start mongodb

echo "                                                         Finished Mongodb Install"

mkdir -p ~/opt/Docker/mongodb
mkdir -p ~/opt/Docker/influxdb
mkdir -p ~/opt/Docker/grafana

sudo docker run -d --restart always -p 27017:27017 --name=mongo -v ~/opt/Docker/mongodb:/data/db mongo
sudo docker run -d --restart always -p 8086:8086 --name=influxdb -v ~/opt/Docker/influxdb:/var/lib/influxdb influxdb
sudo docker run -d --restart always -p 3000:3000 --name=grafana --user 1000:1000 -v ~/opt/Docker/grafana:/var/lib/grafana grafana/grafana

cd /opt

sudo git clone https://github.com/5g-empower/empower-runtime

sudo ln -sf /opt/empower-runtime/conf /etc/empower

sudo mkdir -p /var/www/

sudo ln -sf /opt/empower-runtime/webui /var/www/empower

echo "Run <sudo python3 /opt/empower-runtime/empower-runtime.py> to start the Empower Controller"
