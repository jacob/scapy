#!/bin/bash 
sudo apt update
sudo apt install -y build-essential cmake git libboost-all-dev libopencv-dev python3-pip swig
sudo pip3 install pybombs
pybombs auto-config
pybombs recipes add-defaults
pybombs prefix init ~/prefix-3.9 -R gnuradio39
source ~/prefix-3.9/setup_env.sh
gnuradio-companion

git clone https://github.com/bastibl/gr-foo.git
git checkout maint-3.8
cd gr-foo
mkdir build
cd build
cmake ..
make
sudo make install
sudo ldconfig

cd

git clone git@github.com:bastibl/gr-ieee802-15-4.git
git checkout maint-3.8
cd gr-ieee802-11
mkdir build
cd build
cmake ..
make
sudo make install
sudo ldconfig

cd

sudo sysctl -w kernel.shmmax=2147483648
